<p align="center">![logo](img/aura.png)</p>

## Screenshots
![Screenshot](img/screenshot.png)

An easily editable, super-minimal info script written in bash.

## What's New
+ You can set colors at runtime with the -c flag
+ Added runtime configuration with -n flag
+ Added usage example with -h flag
+ Support for non-EWMH window managers
+ Improved terminal detection (by [@sjugge](https://gitlab.com/sjugge))
+ Fixed width output and RPM support (by [@awmyhr](https://gitlab.com/awmyhr))
+ Cummulative package count, so multiple package managers work together

## Installation

### Arch Linux
Aurafetch is available from the AUR as [aurafetch-git](https://aur.archlinux.org/packages/aurafetch-git/)

1. `git clone https://aur.archlinux.org/aurafetch-git.git`
2. `cd aurafetch-git`
3. `makepkg -si`

### Manual installation
If you prefer DIY (or you don't have access to the AUR), you can install it manually:

First, install `xprop`, it is used to find the terminal name and the window manager's name.

1. Clone the repo: `git clone https://gitlab.com/LionessAlana/aurafetch.git/`
2. Move to /usr/bin: `cd /usr/bin/`
3. Create symlink: `sudo cp -s ~/aurafetch/aura ./`
4. Run aura: `aura`
5. ????
6. **Profit.**

*Note: If you clone the repo into a directory other than your home directory, you'll have to change the path in step 3.* 

## Update
If you got Aurafetch from the AUR:<br>
To manually update, you can `cd` into the aurafetch-git dir and `makepkg -si` again.

If you cloned the repo:<br>
Because we used a symlink, you should just be able to cd into the 
aurafetch directory and `git pull`

## Configuring
To configure the output at runtime, you can use the `-n` flag. <br> 
Example: `aura -n 'user colors'` <br>
This will remove the username and color dots from the output.
You can use `aura -h` to view the module names.

You can configure the colors in a similar way with the `-c`flag.<br>
Example: `aura -c 'c1 c3'`<br>
This will display the title in your terminal's color1, and the info in your terminal's color3.
You can check this with `aura -h`

This allows you to create an alias for your favorite output,
such as: `alias aura="aura -n 'user packages' -c 'c3 c1"`

Alternatively, if you clone/fork the repo you can hack until your heart is content!

## Known Issues
+ Support for Debian is a work in progress.
